package controller;

import data.Document;
import java.util.List;
import javax.ejb.Local;

@Local
public interface ControllerLocal {
    
    public String upload(String filename, byte[] file, String description);
    
    public List<Document> print();
    
    public String delete(int idDokumentu);  
    
}
