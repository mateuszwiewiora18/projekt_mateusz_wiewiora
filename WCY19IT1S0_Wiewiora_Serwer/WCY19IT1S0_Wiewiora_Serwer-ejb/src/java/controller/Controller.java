package controller;

import data.Document;
import data.HibernateUtil;
import java.util.List;
import javax.ejb.Singleton;
import org.hibernate.Query;
import org.hibernate.Session;

@Singleton
public class Controller implements ControllerLocal {

    Session dbSession = HibernateUtil.getSessionFactory().getCurrentSession();

    @Override
    public String upload(String filename, byte[] file, String description) {
        Boolean isSuccess;
        String responseMessage = "Error adding document!";

        try {
            Document doc = new Document();
            doc.setFilename(filename);
            doc.setFile(file);
            doc.setDescription(description);

            dbSession = HibernateUtil.getSessionFactory().getCurrentSession();
            dbSession.beginTransaction();
            dbSession.persist(doc);
            dbSession.getTransaction().commit();
            isSuccess = true;
        } catch (Exception ex) {
            isSuccess = false;
            ex.printStackTrace();
        }

        if (isSuccess) {
            responseMessage = ("Document " + filename + " added successfully");
        }
        return responseMessage;
    }

    @Override
    public List<Document> print() {
        try {
            dbSession = HibernateUtil.getSessionFactory().getCurrentSession();
            dbSession.beginTransaction();
            Query q = dbSession.createQuery("from Document");
            List<Document> ret = q.list();
            dbSession.getTransaction().commit();
            return ret;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String delete(int docId) {
        String responseMessage = "Error adding file!";
        boolean isSuccess = false;

        try {
            dbSession = HibernateUtil.getSessionFactory().getCurrentSession();
            dbSession.beginTransaction();
            Query q = dbSession.createQuery("delete from Document where id = :docId").setInteger("docId", docId);
            q.executeUpdate();
            dbSession.getTransaction().commit();
            isSuccess = true;
        } catch (Exception ex) {
            isSuccess = false;
            ex.printStackTrace();
        }
        
        if (isSuccess) {
            responseMessage = "Document deleted successfully";
        }

        return responseMessage;
    }
}
