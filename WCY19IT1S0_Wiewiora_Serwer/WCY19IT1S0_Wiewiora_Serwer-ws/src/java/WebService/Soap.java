package WebService;

import data.Document;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import controller.ControllerLocal;

@WebService(serviceName = "Soap")
public class Soap {

    @EJB
    private ControllerLocal ejbRef;

    @WebMethod(operationName = "upload")
    public String upload(@WebParam(name = "filename")String filename, @WebParam(name = "file")byte[] file, @WebParam(name = "description")String description) {
        return ejbRef.upload(filename, file, description);
    }
    
    @WebMethod(operationName = "print")
    public List<Document> print() {
        return ejbRef.print();
    }
    
    @WebMethod(operationName = "delete")
    public String delete(@WebParam(name = "docId")int docId) {
        return ejbRef.delete(docId);
    }     
}