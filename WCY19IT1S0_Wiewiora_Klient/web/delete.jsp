<%-- 
    Document   : delete
    Created on : 2022-06-18, 21:13:55
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document deleted</title>
    </head>
    <body style="max-width: 1366px; margin: 0 auto;">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
            <h1 style="display: flex; align-items: center; justify-content: center; margin-top: 50px; font-family: 'Calibri';"></h1>
            <div style="margin: 0 auto; justify-content: center; text-align: center; font-family: 'Calibri'; font-size: 20px; padding: 20px 10px 20px 10px; width: 50%;">

                Document has been deleted!

                <form action="list.jsp" class=\'customform\'>
                    <div style="display: flex; align-items: center; justify-content: center;">
                    <button type="submit" style="font-size: 16px; width: 200px; margin-top: 20px; height: 40px;">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;Powrót
                    </button>
                    </div>
                </form>
            </div>
        </body>
</html>
