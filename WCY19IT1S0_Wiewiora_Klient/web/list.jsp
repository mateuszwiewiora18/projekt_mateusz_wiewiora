<%@page import="org.apache.xml.security.utils.Base64"%>
<%@page import="sun.misc.BASE64Encoder"%>
<%@page import="java.util.List"%>
<%@page import="webservice.Document"%>
<%@page import="webservice.Soap_Service"%>
<%@page import="webservice.Soap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>XLS Documents list</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            html{
                background-color: #dfd;
            }
        </style>
    </head>
    <%
        Soap soap = new Soap_Service().getSoapPort();
        List<Document> docs = soap.print();
    %>
    <body style="max-width: 1366px; margin: 0 auto; margin-top: 50px; font-family: Calibri;">
        <div id="accordion">
            <%
                for (Document d : docs) {
                    int docId = d.getId();
                    String fileName = d.getFilename();
                    String description = d.getDescription();
                    String base64 = Base64.encode(d.getFile());
            %>
            <div class="card">
                <div class="card-header" id="heading<%=docId%>">
                    <h1>
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<%=docId%>" aria-expanded="false" aria-controls="collapse<%=docId%>" 
                                style="width: 75%; height: 100%; text-align: left; font-size: 24px; color: black;"><%=fileName%>
                        </button>

                        <a href="data:octet-stream;base64,<%=base64%>" download="<%=fileName%>">
                            <button id="downloadBtn" style="font-size: 18px; width: 125px; box-shadow:inset 0px 0px 0px -4px #caefab; border: 1px solid black; color: black; height: 2.7rem; cursor:pointer; margin-top: -8px;">
                                Download
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </button>
                        </a>

                        <form method="post" action="Delete" style="width: 10%; float: right;">
                            <input type="hidden" id="docId" name="docId" value="<%=docId%>">
                            <button id="delBtn" style="font-size: 18px; width: 125px; box-shadow:inset 0px 0px 0px -4px #f5978e; border: 1px solid black;
                                    color: black; height: 2.7rem; cursor:pointer; margin-top: -8px;">Delete&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </h1>
                </div>

                <div id="collapse<%=docId%>" class="collapse show" aria-labelledby="heading<%=docId%>" data-parent="#accordion">
                    <div class="card-body" style="display: flex; align-items: center;">
                        <div style="width: 10%; font-size: 24px; border-right: 2px double #D6D6D6;">Description</div>
                        <div style="width: 80%; padding-left: 4%; padding-right: 4%; font-size: 18px;">
                            <%=description%>
                        </div>
                    </div>
                </div>
            </div>
            <% }%>
        </div>

        <form action="index.html" class="customform" style="background-color: #dfd;">
            <div style="display: flex; align-items: center; justify-content: center;">
                <button type="submit" style="font-size: 16px; width: 200px; margin-top: 20px; height: 40px;">
                    <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;Powrót</button>
            </div>
        </form>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
