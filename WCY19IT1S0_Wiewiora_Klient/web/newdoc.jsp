<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Upload XLS document</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            html{
                background-color: #dfd;
            }
        </style>
    </head>
    <body style="max-width: 1366px; margin: 0 auto; margin-top: 50px; font-family: Calibri; text-align: center">
        <div style="border: 1px outset #999999; padding: 20px 50px 20px 50px;background-color: #aea">
            <div class="custom-file">
                <form method="post" action="NewdocAfter" enctype="multipart/form-data" style="width: 100%;">
                    <input style="border: 1px outset #999999; padding: 10px; position: relative; width: 70%; margin-top: 20px;" type="file" name="addFile" id="addFile" accept=".xls,.xlsx"/><br><br>
                    <input type="text" name="addDescription" id="addDescription" required placeholder="Description" style="width: 70%; height: 40px;"></input><br><br>
                    <button type="submit" value="Add file" style="font-size: 30px; width: 29.6%; box-shadow:inset 0px 1px 8px -3px #bee2f9; border: 1px solid black; color: black; cursor:pointer; height: 1.80em;">Add file&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-upload" aria-hidden="true"></i></button>
                </form>
                <br/>
            </div>
            <form action="index.html" class='customform'>
                <div style="display: flex; align-items: center; justify-content: center;">
                    <button type="submit" style="font-size: 16px; width: 200px; height: 40px; margin-top: 40px;">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;Back
                    </button>
                </div>
            </form>
        </div>
    </body>
</body>
</html>
