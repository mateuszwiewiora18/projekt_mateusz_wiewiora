package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import webservice.Soap;
import webservice.Soap_Service;

@WebServlet(name = "NewdocAfter", urlPatterns = {"/NewdocAfter"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
public class NewdocAfter extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //upload document
        Soap_Service service = new Soap_Service();
        Soap port = service.getSoapPort();
        
        //get submitted file
        Part docFile = request.getPart("addFile");
        int fileSize = (int)docFile.getSize();
        
        //get document information
        String filename = docFile.getSubmittedFileName();
        String description = request.getParameter("addDescription");
        
        byte[] content = new byte[fileSize];
        docFile.getInputStream().read(content);
        
        String message = port.upload(filename, content, description);
        
        //set response message
        request.setAttribute("message", message);
        
        //forward to confirmation page
        getServletContext().getRequestDispatcher("/added.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
