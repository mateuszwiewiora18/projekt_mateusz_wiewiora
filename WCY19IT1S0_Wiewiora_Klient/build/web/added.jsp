<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document added</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style>
            html{
                background-color: #dfd;
            }
        </style>
    </head>
    <body style="max-width: 1366px; margin: 0 auto; background-color: #dfd;">
        <div style="border: 1px outset #999999; padding: 20px 50px 20px 50px;background-color: #aea; margin: 50px auto; justify-content: center; text-align: center; font-family: 'Calibri'; font-size: 20px; padding: 20px 10px 20px 10px; width: 50%;">
            ${message}
            <form action="index.html" class="customform">
                <div style="display: flex; align-items: center; justify-content: center;">
                    <button type="submit" style="font-size: 16px; width: 200px; margin-top: 20px; height: 40px;">
                        <i class="fa fa-chevron-left fa-lg"></i>&nbsp;&nbsp;&nbsp;Back
                    </button>
                </div>
            </form>
        </div>
    </body>
</html>
